/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 10_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <stdbool.h>
#include "catDatabase.h"

int addCat(char name[], int gender, int breed, bool isFixed, float weight);




