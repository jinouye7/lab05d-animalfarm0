/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 10_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once
#include <stdbool.h>
#define MAXLENGTH 30
#define MAXCATS 30

enum GENDER { UNKNOWN_GENDER = 0, MALE = 1, FEMALE = 2 };

enum BREED {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};


extern int currentNumberCats;

extern char arrName[MAXCATS][MAXLENGTH];

extern enum GENDER arrGender[MAXCATS];

extern enum BREED arrBreed[MAXCATS];

extern bool arrIsFixed[MAXCATS];

extern float arrWeight[MAXCATS];




