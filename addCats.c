/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 10_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "catDatabase.h"
#include "addCats.h"
//#define DEBUG

int addCat(char name[], int gender, int breed, bool isFixed, float weight){
   if (currentNumberCats > MAXCATS){
      printf("number of cats exceeded\n");
      return 0;
   }

   if (strlen(name) == 0){
      printf("cat's name cannot be empty\n");
      return 0;
   }
   
   if (strlen(name) > MAXLENGTH){
      printf("name must not be > 30 characters\n");
      return 0;
   } 

   if (weight <= 0 ) {
      printf("weight must be > 0\n");
      return 0;
   }
   
   int i = 0;

   for (i = 0; i <= currentNumberCats; ++i){
      
      if (strcmp(name, arrName[i]) == 0){
         printf("name of cat must be unique\n");
         return 0;
      }
   }
   

   strcpy(arrName[currentNumberCats],name);
   arrGender[currentNumberCats] = gender;
   arrBreed[currentNumberCats] = breed;
   arrIsFixed[currentNumberCats] = isFixed;
   arrWeight[currentNumberCats] = weight;
   

   #ifdef DEBUG
      printf("the current number of cats is %d\n", currentNumberCats);
      printf("the maxlength define is still %d\n", MAXLENGTH);
      printf("the name of the cat is %s\n", name);
      printf("the name stored is %s\n", arrName[currentNumberCats]);
      printf("the gender is %d\n", gender);
      printf("the gender stored is %d\n", arrGender[currentNumberCats]);
      printf("the breed is %d\n", breed);
      printf("the breed stored is %d\n", arrBreed[currentNumberCats]);
      printf("the cat was fixed: %d\n", isFixed);
      printf("the cat was fixed stored: %d\n", arrIsFixed[currentNumberCats]);
      printf("the weight of the cat stored is %f\n", weight);
      printf("the weight of the cat stored is %f\n", arrWeight[currentNumberCats]);
   #endif 

      currentNumberCats += 1;

      printf("cat added\n");
      return 1;



}





