/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 10_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "catDatabase.h"
#include <stdbool.h>

int currentNumberCats = 0;

char arrName[MAXCATS][MAXLENGTH];

enum GENDER arrGender[MAXCATS];

enum BREED arrBreed[MAXCATS];

bool arrIsFixed[MAXCATS];

float arrWeight[MAXCATS];




