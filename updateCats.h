/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 14_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//


int updateCatName(int index, char newName[]);
int fixCat(int index);
int updateCatWeight(int index, float newWeight);

 

