/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 14_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "catDatabase.h"
#include "updateCats.h"

int updateCatName(int index, char newName[]){
   if (index >= currentNumberCats){
      printf("error: no cat at index %d\n", index);
      return 1;
   }
   if (index < 0){
      printf("error: index must be greater than 0\n");
      return 1;
   }
   if (strlen(newName) == 0){
      printf("error: cat's name cannot be empty\n");
      return 1;
   }
   
   if (strlen(newName) > MAXLENGTH){
      printf("error: name must not be > 30 characters\n");
      return 1;
   } 


   int i = 0;

   for (i = 0; i <= currentNumberCats; ++i){

      if (strcmp(newName, arrName[i]) == 0){
         printf("error: new name of cat must be unique\n");
         return 1;
      }
   }
   
   strcpy(arrName[index],newName);

   printf("The name of the cat at index %d has been changed to %s\n", index, newName);
   
   return 0;

}



int fixCat(int index){
   if (index >= currentNumberCats){
      printf("error: no cat at index %d\n", index);
      return 1;
   }
   if (index < 0){
      printf("error: index must be greater than 0\n");
      return 1;
   }
   if(arrIsFixed[index] == true){
      printf("cat is already fixed\n");
      return 0;
   }

   arrIsFixed[index] = true;

   printf("cat at index %d is now fixed\n", index);
   return 0;
}



int updateCatWeight(int index, float newWeight){
   if (index >= currentNumberCats){
      printf("error: no cat at index %d\n", index);
      return 1;
   }
   if (index < 0){
      printf("error: index must be greater than 0\n");
      return 1;
   }

   if(newWeight <= 0){
      printf("error: cat weight must be greater than 0\n");
      return 1;
   }

   arrWeight[index] = newWeight;
   printf("weight of cat at index %d has been changed to %f\n", index, arrWeight[index]);
   return 0;

}
