/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 11_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "catDatabase.h"
#include <stdio.h>
#include <string.h>

int printCat(int index){
   //print if cat is not in database
   if (index < 0){
      printf("animalFarm0: Bad cat [%d]\n", index);
      return 1;
   }
   if (index >= currentNumberCats){
      printf("animalFarm0: Bad cat [%d]\n", index);
      return 1;
   }

   //print if cat is in database
   printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", 
         index, arrName[index], arrGender[index], arrBreed[index], arrIsFixed[index], arrWeight[index]);



   //printf("the print thing is working\n");
   return 0;
}



int printAllCats(){
   if (currentNumberCats == 0){
      printf("there are no cats\n");
   }
   int i = 0;
   for (i = 0; i < currentNumberCats; ++i){
      printf("cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n",
         i, arrName[i], arrGender[i], arrBreed[i], arrIsFixed[i], arrWeight[i]);
   }
   //printf("print all is going\n");
   return 0;
}



int findCat(char name[]){

   int i = 0;

   if (strlen(name) == 0){
      printf("error: cat's name cannot be empty\n");
      return 0;
   }
   
   if (strlen(name) > MAXLENGTH){
      printf("error: name must not be > 30 characters\n");
      return 0;
   } 

   for (i = 0; i < currentNumberCats; ++i){

      if (strcmp(name, arrName[i]) == 0){

         printf("cat match at index %d\n", i);
         return i;
      }
    
   }
   printf("error: cat is not there\n");
   return 1;
   
}
 

