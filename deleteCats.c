/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 14_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <string.h>
#include <stdio.h>
#include "deleteCats.h"
#include "catDatabase.h"

int deleteAllCats(){
   memset(arrName, 0, sizeof arrName);
   memset(arrGender, 0, sizeof arrGender);
   memset(arrBreed, 0, sizeof arrBreed);
   memset(arrIsFixed, 0, sizeof arrIsFixed);
   memset(arrWeight, 0, sizeof arrWeight);
   currentNumberCats = 0;
   printf("all cats deleted\n");
   return 0;
}


 

